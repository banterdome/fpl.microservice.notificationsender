﻿namespace FPL.Microservice.NotificationSender.Configuration
{
    public class FirebaseSettings
    {
        //workaround for being over environment variable length
        public string ConnectionDetails1 { get; set; }
        public string ConnectionDetails2 { get; set; }
    }
}
