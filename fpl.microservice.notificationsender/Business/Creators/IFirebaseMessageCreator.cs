﻿namespace FPL.Microservice.NotificationSender.Business.Creators
{
    using FirebaseAdmin.Messaging;

    public interface IFirebaseMessageCreator
    {
        Message Create(string title, string body, string token);
    }
}
