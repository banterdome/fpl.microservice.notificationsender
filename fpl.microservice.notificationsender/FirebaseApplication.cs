﻿using FirebaseAdmin.Messaging;
using System.Threading.Tasks;

namespace FPL.Microservice.NotificationSender
{
    public class FirebaseApplication : IFirebaseApplication
    {
        public async Task SendMessage(Message message)
        {
            try
            {
                var firebase = FirebaseMessaging.DefaultInstance;
                await firebase.SendAsync(message); //to do: send all async and see how invalid tokens are handled
            }
            catch
            {
                //should remove the token from the db at some point if this fails, but probably need to change the schema if this microservice is generic!
                //for now we will swallow though
            }
        }
    }
}