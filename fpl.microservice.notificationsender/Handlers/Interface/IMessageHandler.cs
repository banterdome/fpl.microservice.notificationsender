﻿namespace FPL.Microservice.NotificationSender.Handlers.Interface
{
    using System.Threading.Tasks;

    public interface IMessageHandler<T> where T : class
    {
        Task Handle(T message);
    }
}
