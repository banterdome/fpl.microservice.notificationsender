﻿namespace FPL.Microservice.NotificationSender.Business.Creators
{
    using FirebaseAdmin.Messaging;

    public class FirebaseMessageCreator : IFirebaseMessageCreator
    {
        public Message Create(string title, string body, string token)
        {
            return new Message()
            {
                Notification = new Notification
                {
                    Title = title,
                    Body = body,
                },
                Token = token
            };
        }
    }
}
