﻿using FirebaseAdmin.Messaging;
using System.Threading.Tasks;

namespace FPL.Microservice.NotificationSender
{
    public interface IFirebaseApplication
    {
        Task SendMessage(Message message);
    }
}