﻿namespace FPL.Microservice.NotificationSender.IoC
{
    using System.Diagnostics.CodeAnalysis;
    using System.IO;
    using Autofac;
    using FPL.Microservice.NotificationSender.Configuration;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Logging;

    [ExcludeFromCodeCoverage]
    public class ServiceModule : ModuleBase
    {
        public ServiceModule(IConfiguration configuration) : base(configuration)
        {
        }

        protected override void Load(ContainerBuilder builder)
        {
            // builds temporary environment variable configuration
            var ec = new ConfigurationBuilder()
                     .AddEnvironmentVariables()
                     .Build();

            base.Load(builder);


            builder.RegisterInstance(new LoggerFactory())
                         .As<ILoggerFactory>();

            builder.RegisterGeneric(typeof(Logger<>))
                   .As(typeof(ILogger<>))
                   .SingleInstance();

            builder.Register(k => this.GetFirebaseSettings(ec))
                   .SingleInstance();

            builder.Register(k => this.GetQueueSettings(ec))
                  .SingleInstance();
        }

        private FirebaseSettings GetFirebaseSettings(IConfigurationRoot ec)
        {
            // maps values to the selected type
            var result = ec.Get<FirebaseSettings>();

            return result;
        }

        private QueueSettings GetQueueSettings(IConfigurationRoot ec)
        {
            // maps values to the selected type
            var result = ec.Get<QueueSettings>();

            return result;
        }
    }
}
