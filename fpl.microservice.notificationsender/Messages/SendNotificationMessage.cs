﻿namespace FPL.Microservice.NotificationSender.Messages
{
    using System;
    using System.Collections.Generic;

    public class SendNotificationMessage
    {
       public string Title { get; set; }
       public string Body { get; set; }
       public List<string> Tokens { get; set; }
       public bool Retry { get; set; }
    }
}
