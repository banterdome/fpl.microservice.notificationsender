﻿namespace FPL.Microservice.NotificationSender.IoC
{
    using Autofac;
    using Microsoft.Extensions.Configuration;
    using System.IO;
    using System.Reflection;
    using Microsoft.Extensions.Configuration.Json;
    using Microsoft.Extensions.Logging;

    public class ModuleBase : Autofac.Module
    {
        protected readonly IConfiguration Configuration;

        public ModuleBase(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        protected override void Load(ContainerBuilder builder)
        { 
            base.Load(builder);
            var assembly = Assembly.GetExecutingAssembly();

            builder.RegisterAssemblyTypes(assembly).Where(t => t.Name.EndsWith("Handler")).AsImplementedInterfaces();
            builder.RegisterAssemblyTypes(assembly).Where(t => t.Name.EndsWith("Getter")).AsImplementedInterfaces();
            builder.RegisterAssemblyTypes(assembly).Where(t => t.Name.EndsWith("Creator")).AsImplementedInterfaces();
            builder.RegisterAssemblyTypes(assembly).Where(t => t.Name.EndsWith("Application")).AsImplementedInterfaces();

            builder.RegisterType<Service>();
        }
    }
}
