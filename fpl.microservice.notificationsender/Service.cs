﻿namespace FPL.Microservice.NotificationSender
{
    using FirebaseAdmin;
    using FPL.Microservice.NotificationSender.Configuration;
    using FPL.Microservice.NotificationSender.Handlers.Interface;
    using FPL.Microservice.NotificationSender.Messages;
    using Google.Apis.Auth.OAuth2;
    using Microsoft.Extensions.Logging;
    using Newtonsoft.Json;
    using RabbitMQ.Client;
    using RabbitMQ.Client.Events;
    using System;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;

    public class Service
    {
        private readonly ILogger<Service> logger;
        private readonly IMessageHandler<SendNotificationMessage> messageHandler;
        private readonly FirebaseSettings firebaseSettings;
        private readonly QueueSettings queueSettings;

        public Service(ILogger<Service> logger, IMessageHandler<SendNotificationMessage> messageHandler, FirebaseSettings firebaseSettings, QueueSettings queueSettings)
        {
            this.logger = logger;
            this.messageHandler = messageHandler;
            this.firebaseSettings = firebaseSettings;
            this.queueSettings = queueSettings;
        }

        public async Task Run()
        {
            logger.LogInformation("Starting NotificationSender Microservice");
            try
            {
                logger.LogInformation("Connecting to firebase");
                var defaultApp = FirebaseApp.Create(new AppOptions()
                {
                    Credential = GoogleCredential.FromJson(firebaseSettings.ConnectionDetails1 + firebaseSettings.ConnectionDetails2)
                });
            }
            catch
            {
                logger.LogError("Failed to connect to Firebase, exiting...");
                return;
            }

            try
            {
                var factory = new ConnectionFactory() { HostName = queueSettings.HostName, UserName = queueSettings.QueueUsername, Password = queueSettings.Password };
                using (var connection = factory.CreateConnection())
                {
                    using (var channel = connection.CreateModel())
                    {
                        channel.QueueDeclare(queue: queueSettings.QueueName,
                                             durable: false,
                                             exclusive: false,
                                             autoDelete: false,
                                             arguments: null);

                        var consumer = new EventingBasicConsumer(channel);
                        consumer.Received += async (model, ea) =>
                        {
                            SendNotificationMessage message;
                            try
                            {
                                //Byte array, to string, to message object
                                message = JsonConvert.DeserializeObject<SendNotificationMessage>(Encoding.UTF8.GetString(ea.Body.ToArray()));

                                try
                                {
                                    logger.LogInformation($"Handling message {ea.BasicProperties.MessageId}: {message.Title}");
                                    await messageHandler.Handle(message);
                                   
                                    //acknowledge delivery when message handling is complete
                                    channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
                                }
                                catch
                                {
                                    this.logger.LogError($"An error ocurred while handling message {ea.BasicProperties.MessageId}");

                                    if (!message.Retry)
                                    {
                                        this.logger.LogInformation($"Message marked not to retry {ea.BasicProperties.MessageId}");
                                        channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
                                    }
                                    else
                                    {
                                        this.logger.LogInformation($"Message marked to retry {ea.BasicProperties.MessageId}, requeuing...");
                                        channel.BasicNack(deliveryTag: ea.DeliveryTag, false, true);
                                    }
                                }
                            }
                            catch
                            {
                                this.logger.LogError($"ID: {ea.BasicProperties.MessageId} does not have a valid body, marking complete");
                                channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
                            }     
                        };

                        channel.BasicConsume(queue: queueSettings.QueueName, autoAck: false, consumer: consumer);
                        Thread.Sleep(Timeout.Infinite);
                    }
                }
            }
            catch(Exception e)
            {
                this.logger.LogError($"An error ocurred, while connecting to queue, shutting down {e.Message}");
            }
        }
    }
}