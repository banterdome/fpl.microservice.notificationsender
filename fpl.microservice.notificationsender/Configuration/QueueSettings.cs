﻿namespace FPL.Microservice.NotificationSender.Configuration
{
    public class QueueSettings
    {
        public string HostName { get; set; }
        public string QueueUsername { get; set; }
        public string Password { get; set; }
        public string QueueName { get; set; }
    }
}