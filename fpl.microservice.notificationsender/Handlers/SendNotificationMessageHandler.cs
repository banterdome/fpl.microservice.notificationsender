﻿namespace FPL.Microservice.NotificationSender.Handlers
{
    using FPL.Microservice.NotificationSender.Handlers.Interface;
    using FPL.Microservice.NotificationSender.Messages;
    using System.Threading.Tasks;
    using FPL.Microservice.NotificationSender.Business.Creators;

    public class SendNotificationMessageHandler : IMessageHandler<SendNotificationMessage>
    {
        private readonly IFirebaseMessageCreator firebaseMessageCreator;
        private readonly IFirebaseApplication firebaseApplication;
        public SendNotificationMessageHandler(IFirebaseApplication firebaseApplication, IFirebaseMessageCreator firebaseMessageCreator)
        {
            this.firebaseMessageCreator = firebaseMessageCreator;
            this.firebaseApplication = firebaseApplication;
        }
        public async Task Handle(SendNotificationMessage message)
        {  
            foreach (var token in message.Tokens)
            {
                var firebaseMessage = this.firebaseMessageCreator.Create(message.Title, message.Body, token);

                await firebaseApplication.SendMessage(firebaseMessage);
            }
        }
    }
}