FROM mcr.microsoft.com/dotnet/sdk:5.0 AS builder
WORKDIR /src
EXPOSE 80
COPY . .

#Restore Dependencies
RUN dotnet restore "fpl.microservice.notificationsender/FPL.Microservice.NotificationSender.csproj" -s "https://www.nuget.org/api/v2/"
RUN dotnet restore "FPL.Microservice.NotificationSender.Tests/FPL.Microservice.NotificationSender.Tests.csproj" -s "https://www.nuget.org/api/v2/" 
#Run Tests
RUN dotnet test "FPL.Microservice.NotificationSender.Tests/FPL.Microservice.NotificationSender.Tests.csproj" /p:CollectCoverage=true
#Publish App
RUN dotnet publish "fpl.microservice.notificationsender/FPL.Microservice.NotificationSender.csproj" -c Release -o /app/publish
FROM mcr.microsoft.com/dotnet/sdk:5.0  AS final
#Copy App and Run
WORKDIR /app
ENV ASPNETCORE_URLS=http://+:80
COPY --from=builder /app/publish /app
ENTRYPOINT ["dotnet", "FPL.Microservice.NotificationSender.dll"]
