﻿namespace FPL.Microservice.NotificationSender 
{
    using Autofac;
    using FPL.Microservice.NotificationSender.IoC;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Logging;
    using System.IO;
    using System.Threading.Tasks;

    class Program
    {

        private static IContainer CompositionRoot()
        {
            var configBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false);

            IConfiguration config = configBuilder.Build();

            var builder = new ContainerBuilder();

            builder.RegisterModule(new ServiceModule(config));
            return builder.Build();
        }

        static async Task Main(string[] args)
        {
            await CompositionRoot().Resolve<Service>().Run();
        }
    }
}
