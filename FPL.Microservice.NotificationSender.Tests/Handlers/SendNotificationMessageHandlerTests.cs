﻿namespace FPL.Microservice.NotificationSender.Tests.Handlers
{
    using FirebaseAdmin.Messaging;
    using FPL.Microservice.NotificationSender.Business.Creators;
    using FPL.Microservice.NotificationSender.Handlers;
    using FPL.Microservice.NotificationSender.Messages;
    using Moq;
    using Moq.AutoMock;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Xunit;

    public class SendNotificationMessageHandlerTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public async Task HandleHandlesMessageCorrectly()
        {
            
            var stubMessage = new SendNotificationMessage() { Tokens = new List<string> { "hello", "test" }, Body = "TestBody", Title = "Message" };
            var stubFirebaseMessage =  new Message(){Notification = new Notification{ Title = "Message",Body = "TestBody"}, Token = "token"};

            this.autoMocker.GetMock<IFirebaseMessageCreator>()
                .Setup(x => x.Create("Message", "TestBody", It.IsAny<string>()))
                .Returns(stubFirebaseMessage);
      
            var sut = this.autoMocker.CreateInstance<SendNotificationMessageHandler>();

            await sut.Handle(stubMessage);


            this.autoMocker.GetMock<IFirebaseApplication>()
                .Verify(x => x.SendMessage(stubFirebaseMessage), Times.Exactly(2));
        }
    }
}
